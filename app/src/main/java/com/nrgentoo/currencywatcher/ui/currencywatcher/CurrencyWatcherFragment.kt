package com.nrgentoo.currencywatcher.ui.currencywatcher

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nrgentoo.currencywatcher.R

class CurrencyWatcherFragment : Fragment() {

    companion object {
        fun newInstance() = CurrencyWatcherFragment()
    }

    private lateinit var viewModel: CurrencyWatcherViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.currency_watcher_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(CurrencyWatcherViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
