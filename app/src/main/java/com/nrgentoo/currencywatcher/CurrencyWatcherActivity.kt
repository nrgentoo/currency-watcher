package com.nrgentoo.currencywatcher

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.nrgentoo.currencywatcher.ui.currencywatcher.CurrencyWatcherFragment

class CurrencyWatcherActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.currency_watcher_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, CurrencyWatcherFragment.newInstance())
                .commitNow()
        }
    }

}
